#!/usr/bin/env python3

import os
import shutil
import subprocess
import sys
import re
from os.path import join as joinpath
import json
import requests

import cssgen

def article_exists(config, article):
    conn = requests.get(config["article_lookup_url"] % article)
    code = conn.status_code
    if str(code)[0] == "2":
        return True
    elif str(code)[0] == "3":
        raise "article_lookup_url is not configured properly: Getting this response code "+str(code)
    elif str(code)[0] == "4":
        return False
    else:
        raise "Unknown HTTP return code: "+str(code)

with open("config.json","r") as f:
    config = json.loads(''.join(f.readlines()))

def getPoster(production_name,files):
    regex = re.compile("(?i)%s\.(?:png|jpg|jpeg)"%re.escape(production_name))
    posters = list(filter(regex.fullmatch, files))
    if len(posters) == 0:
        return None
    elif len(posters) > 1:
        print("Warnung: Für die Produktion \"%s\" sind mehrere Plakate vorhanden. Es wird die Datei \"%s\" verwendet."%(production_name,posters[0]))
    return posters[0]

### Entpacken
extracted_files = os.listdir("data/input")
if 'plakate.txt' not in extracted_files:
    sys.exit("Fehler: Keine 'plakate.txt'-Datei vorhanden.")


### Informationen auslesen
try:
    with open("data/input/plakate.txt","r") as f:
        plakate_txt = [line.strip() for line in f.readlines()]
except Exception as e:
    print("Fehler: Konnte 'plakate.txt' nicht lesen. Ist sie wirklich im Inputordner enthalten?")
    raise e

print("Die folgenden Produktionen sind in der 'plakate.txt'-Datei angegeben: "+", ".join(plakate_txt))

poster_names = dict(zip(plakate_txt,map(lambda p:getPoster(p,extracted_files),plakate_txt)))
no_poster = list(filter(lambda x:poster_names[x] is None,poster_names))
if len(no_poster) > 0:
    print("Für die folgenden Produktionen wurde KEIN Plakat gefunden: "+", ".join(no_poster))
    print("Es werden automatisch Platzhaltergrafiken erstellt.")
else:
    print("Für alle Produktionen sind Plakate vorhanden. Gut.")

unused_files = set(extracted_files) - set(["plakate.txt"]) - set(filter(lambda x:x is not None,poster_names.values()))
if len(unused_files) > 0:
    print("Warnung: die folgenden Dateien finden keine Verwendung:")
    print(unused_files)

print("Überprüfe Wiki-Artikel.")
for name in plakate_txt:
    if article_exists(config, name):
        # print("%s existiert."%name)
        pass
    else:
        sys.exit("Fehler: Es existiert im Wiki kein Artikel mit dem Namen \"%s\"."%name)


### Dinge erstellen und zusammenbauen
print("Dateien werden erstellt.")
try:
    shutil.rmtree(config["build_dir"])
except Exception:
    # need not have existed
    pass
os.mkdir(config["build_dir"])
shutil.copytree("data/static",joinpath(config["build_dir"],"frontpage_data"))

for n, produktion in enumerate(plakate_txt):
    plakatdatei = poster_names[produktion]
    if plakatdatei is None:
        # print("Platzhaltergrafik für %s wird erstellt."%produktion)
        subprocess.check_call(["convert","data/no_poster_yet.png","-gravity","north","-font",config["font"],"-pointsize",str(config["pointsize"]),"-draw","text 0,40 '%s'"%produktion,"-quality",str(config["quality"]),"-type","Grayscale",joinpath(config["build_dir"],"frontpage_data","plakat%d.jpg"%n)])
    else:
        # print("Plakat für %s wird konvertiert."%produktion)
        subprocess.check_call(["convert",joinpath("data/input",plakatdatei),"-resize","450x450","-quality",str(config["quality"]),joinpath(config["build_dir"],"frontpage_data","plakat%d.jpg"%n)])

# print("Website wird gebaut.")
with open(joinpath(config["build_dir"],"index.html"),"w") as index_html:
    with open("data/header.htm","r") as header:
    	header_content = ''.join(header.readlines())
    header_content = header_content.replace("</style>", cssgen.cssgen(len(plakate_txt))+"</style>")
    index_html.write(header_content)
    for n,produktion in enumerate(plakate_txt):
        data = {'url':config["article_lookup_url"]%produktion,'plakat':"frontpage_data/plakat%d.jpg"%n,'name':produktion}
        index_html.write(config["plakat_html"] % data)
    with open("data/footer.htm","r") as footer:
        shutil.copyfileobj(footer,index_html)

print("""\nFrontseite erfolgreich gebaut. Du kannst sie jetzt begutachten unter
http://thunis.peacock.uberspace.de/frontpage_preview

Wenn alles okay ist, kannst du die Vorschau mit diesem Befehl veröffentlichen:
./activate""")

shutil.rmtree("data/input",ignore_errors=True)
os.mkdir("data/input")
