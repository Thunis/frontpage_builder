Der Frontpage Builder erstellt aus Plakaten eine Website mit Links zu den jeweiligen Wikiseiten.

# Verwaltung

Die Frontpage kann nur vom Admin geändert werden, weil dazu direkter Zugang zum Server erforderlich ist.

# Infos für Regisseure

Um auf der Frontseite verlinkt zu werden, muss bereits ein Wikiartikel zur Produktion vorhanden sein. Dem Admin muss man dann den Namen des Wikiartikels nennen und ggf. erklären, welches Plakat des Wiki-Artikels er verwenden soll. Wenn erst später ein Plakat vorhanden sein wird, wird bis dahin ein Platzhalterplakat verwendet. Sobald dann ein Plakat verfügbar ist, muss der Admin nochmal informiert werden.

# Anleitung für den Admin

## Dateien

Man muss in einem leeren Ordner eine Textdatei (z.B. mit Notepad, nicht mit einem Officeprogramm!) namens "plakate.txt" erstellen (Kleinschreibung beachten!). Diese muss so aussehen:

    Titel der ersten Produktion
    Titel der zweiten Produktion
    Titel der dritten Produktion

usw.

Die angegebenen Titel müssen jeweils dem Namen des zu verknüpfenden Wikiartikels entsprechen. Prinzipiell kann mit dem frontpage_builder alles verlinkt werden, was einen Wikiartikel hat.

Des Weiteren sollte der Ordner Plakate zu möglichst jeder Produktion enthalten. Die Plakate müssen den in der "plakate.txt" angegebenen Produktionstitel als Dateinamen haben, gefolgt von der Endung ".png" oder ".jpg". Ihre längere Seite sollte außerdem mindestens 450 Pixel messen (da sie auf diese Größe skaliert werden). Wenn zu einer Produktion kein Plakat vorhanden ist, wird der frontpage_builder automatisch ein Dummyplakat mit dem Produktionstitel erstellen.

## Online bringen

    scp * thunis@peacock.uberspace.de:~/frontpage_builder/data/input/
    ssh thunis@peacock.uberspace.de
    cd frontpage_builder
    ./builder

Die Ausgabe des letzten Befehls sollte man sorgfältig lesen. Wenn er erfolgreich verläuft, kann man eine Vorschauversion der neuen Frontpage auf http://thunis-uni.de/frontpage_preview ansehen. Falls die Seite wie gewünscht aussieht, kann man sie mit diesem Befehl als neue Frontpage setzen:

    ./activate

