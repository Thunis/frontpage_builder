def formatstyle(minw, maxw, contw, n):
	if maxw:
		maxwstr = " and (max-width: %dpx)"%maxw
	else:
		maxwstr = ""
	return ("""@media (min-width: %dpx)"""+maxwstr+""" {
 #container {
  width: %dpx;
 }
 .poster {
  margin-left:15px;
 }
 li:nth-child(%dn+1) .poster {
  margin-left:0px;
 }
}""")%(minw,contw,n)

def fitting_w(num_blocks):
	return num_blocks * 500 + (num_blocks-1)*15

def cssgen(num_blocks):
	last = num_blocks
	ret = ""
	for fitting_blocks in range(2,last+1):
		content_width = fitting_w(fitting_blocks)
		needed_space = content_width + 50 # for scroll bars and body padding
		if fitting_blocks < last:
			next_content_width = fitting_w(fitting_blocks+1)
			next_needed_space_m1 = next_content_width + 50 - 1
		else:
			next_needed_space_m1 = None
		ret += formatstyle(needed_space, next_needed_space_m1, content_width, fitting_blocks) + '\n'
	return ret
